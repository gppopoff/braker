use ggez::conf::{Conf, WindowMode};
use ggez::event;
use ggez::filesystem;
use ggez::graphics;
use ggez::input;
use ggez::mint::Point2;
use ggez::timer;
use ggez::{Context, ContextBuilder, GameResult};

use braker::assets::Assets;
use braker::entities::{Ball, Block, Player};

use std::env;
use std::path;

#[derive(Debug, Default)]
struct InputState {
    movement: f32,
    fire: bool,
}

struct MainState {
    game_over: bool,
    input: InputState,
    player: Player,
    balls: Vec<Ball>,
    blocks: Vec<Block>,
    assets: Assets,
    screen_width: f32,
    screen_height: f32,
    level: i32,
}

fn generate_lever(level: i32) -> Vec<Block> {
    let mut blocks: Vec<Block> = Vec::new();

    match level {
        1 => {
            for row in 0..5 {
                for column in 0..13 {
                    let block = Block::new(
                        Point2 {
                            x: column as f32 * 60.0 + 10.0,
                            y: row as f32 * 20.0,
                        },
                        60.0,
                        20.0,
                        1,
                    );
                    blocks.push(block);
                }
            }
        }
        2 => {
            for row in 0..10 {
                for column in 0..13 {
                    let block = Block::new(
                        Point2 {
                            x: column as f32 * 60.0 + 10.0,
                            y: row as f32 * 20.0,
                        },
                        60.0,
                        20.0,
                        2,
                    );
                    blocks.push(block);
                }
            }
        }
        3 => {
            for row in 0..13 {
                for column in 0..13 {
                    let heath: i32;
                    if row == 12 && (column <= 9 && column >= 3) {
                        heath = 100000;
                    } else {
                        heath = 3;
                    }
                    let block = Block::new(
                        Point2 {
                            x: column as f32 * 60.0 + 10.0,
                            y: row as f32 * 20.0,
                        },
                        60.0,
                        20.0,
                        heath,
                    );
                    blocks.push(block);
                }
            }
        }
        _ => {}
    }

    return blocks;
}

impl MainState {
    fn new(ctx: &mut Context, conf: &Conf) -> GameResult<MainState> {
        let assets = Assets::new(ctx)?;
        let screen_width = conf.window_mode.width;
        let screen_height = conf.window_mode.height;

        // Player starts in bottom-middle of the screen
        let player_pos = Point2 {
            x: screen_width / 2.0,
            y: screen_height,
        };

        // Create ball
        let mut balls: Vec<Ball> = Vec::new();

        let ball_pos = Point2 {
            x: player_pos.x - 75.0,
            y: player_pos.y - 80.0,
        };
        let ball = Ball::new(ball_pos, 24.0, 24.0);

        balls.push(ball);

        // Create blocks
        let blocks: Vec<Block> = generate_lever(1);

        let s = MainState {
            game_over: false,
            input: InputState::default(),
            player: Player::new(player_pos),
            balls: balls,
            blocks: blocks,
            assets: assets,
            screen_width: conf.window_mode.width,
            screen_height: conf.window_mode.height,
            level: 1,
        };
        Ok(s)
    }

    // Here we check if ball collides with player platform - if so change direction if the ball - depending of the position of the contact
    fn handle_player_bounce(&mut self) {
        for ball in &mut self.balls {
            let player_bounds = self.player.bounding_rect();
            if player_bounds.contains(ball.pos) {
                //TODO: Add more algebra to bounce - closer to the edge - bigger angle
                if player_bounds.center().x > ball.pos.x {
                    ball.velocity.x = -250.0;
                } else {
                    ball.velocity.x = 250.0;
                }

                ball.velocity.y *= -1.0;
            }
        }
    }
    // For each block check if any ball collides with it - if so do dmg to the block and change direction of the ball
    fn handle_block_bounce(&mut self) {
        for block in self.blocks.iter_mut() {
            let block_bounds = block.bounding_rect();
            for ball in self.balls.iter_mut() {
                if block_bounds.contains(ball.pos) //TODO: Think of better way to detect collision - this way some strange things may happened
                    || block_bounds.contains(Point2 {
                        x: ball.pos.x + ball.get_width(),
                        y: ball.pos.y,
                    })
                    || block_bounds.contains(Point2 {
                        x: ball.pos.x,
                        y: ball.pos.y + ball.get_height(),
                    })
                    || block_bounds.contains(Point2 {
                        x: ball.pos.x + ball.get_width(),
                        y: ball.pos.y + ball.get_height(),
                    })
                {
                    block.deal_dmg(1); //TODO: When we have different balls dmg should be changed
                    if block_bounds.center().x > ball.pos.x {
                        ball.velocity.x = -250.0;
                    } else {
                        ball.velocity.x = 250.0;
                    }
                    ball.velocity.y *= -1.0;
                }
            }
        }
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.blocks.len() == 0 {
            self.level += 1;
            self.blocks = generate_lever(self.level);
        }

        // Check if we have no more balls - game ends
        if self.balls.len() == 0 {
            self.game_over = true;
        }
        if self.game_over {
            return Ok(());
        }
        const DESIRED_FPS: u32 = 60;

        while timer::check_update_time(ctx, DESIRED_FPS) {
            let seconds = 1.0 / (DESIRED_FPS as f32);
            // Player update
            self.player
                .update(self.input.movement, seconds, self.screen_width);

            // Balls update ( ͡° ͜ʖ ͡°)
            for ball in self.balls.iter_mut() {
                ball.update(seconds, self.screen_width.clone());
            }
            self.handle_player_bounce();

            // Blocks update
            self.handle_block_bounce();

            // Remove things that we do not need any more - eg broken blocks or missed balls
            self.balls
                .retain(|ball| ball.is_alive && ball.pos.y <= self.screen_height);
            self.blocks.retain(|block| block.is_alive)
        }
        Ok(())
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: event::KeyCode,
        _keymod: input::keyboard::KeyMods,
        _repeat: bool,
    ) {
        match keycode {
            event::KeyCode::Left => self.input.movement = -1.0,
            event::KeyCode::Right => self.input.movement = 1.0,
            event::KeyCode::Escape => event::quit(ctx),
            event::KeyCode::N => self.blocks = Vec::new(),
            _ => (), // Do nothing
        }
    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        keycode: event::KeyCode,
        _keymod: input::keyboard::KeyMods,
    ) {
        match keycode {
            event::KeyCode::Left | event::KeyCode::Right => self.input.movement = 0.0,
            _ => (), // Do nothing
        }
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let dark_blue = graphics::Color::from_rgb(26, 51, 77);
        graphics::clear(ctx, dark_blue);

        //Check if game is over
        if self.game_over {
            let text = graphics::Text::new(format!("You lost :/"));

            let top_left = Point2 {
                x: (self.screen_width - text.width(ctx)) / 2.0,
                y: (self.screen_height - text.height(ctx)) / 2.0,
            };
            graphics::draw(ctx, &text, graphics::DrawParam::default().dest(top_left))?;
            graphics::present(ctx)?;
            return Ok(());
        }

        // Draw player
        self.player.draw(ctx, &self.assets)?;

        // Draw balls
        for ball in self.balls.iter_mut() {
            ball.draw(ctx, &self.assets)?;
        }

        // Draw blocks
        for block in self.blocks.iter_mut() {
            block.draw(ctx, &self.assets)?;
        }

        graphics::present(ctx);
        Ok(())
    }
}

pub fn main() {
    // Configuration:
    let conf = Conf::new().window_mode(WindowMode {
        min_width: 1024.0,
        min_height: 768.0,
        ..Default::default()
    });

    // Context and event loop
    let (mut ctx, event_loop) = ContextBuilder::new("shooter", "FMI")
        .default_conf(conf.clone())
        .build()
        .unwrap();

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        filesystem::mount(&mut ctx, &path, true);
    }

    // Main loop start
    let state = MainState::new(&mut ctx, &conf).unwrap();
    event::run(ctx, event_loop, state);
}
