use ggez::graphics;
use ggez::mint::{Point2, Vector2};
use ggez::{Context, GameResult};

use crate::assets::Assets;

#[derive(Debug)]
pub struct Player {
    pub pos: Point2<f32>,
}

impl Player {
    pub const SPEED: f32 = 500.0;

    pub fn new(pos: Point2<f32>) -> Self {
        Player { pos }
    }

    pub fn update(&mut self, amount: f32, seconds: f32, max_right: f32) {
        let new_pos = self.pos.x + Self::SPEED * seconds * amount;
        self.pos.x = nalgebra::clamp(new_pos, 0.0, max_right);
    }

    pub fn draw(&self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        let draw_params = graphics::DrawParam::default()
            .dest(self.pos)
            .scale(Vector2 { x: 0.95, y: 0.95 })
            .offset(Point2 { x: 0.5, y: 1.0 });
        graphics::draw(ctx, &assets.ferris_normal_image, draw_params)?;

        Ok(())
    }

    pub fn bounding_rect(&self) -> graphics::Rect {
        let left = self.pos.x - 75.0;
        let right = self.pos.x + 75.0;
        let top = self.pos.y - 40.0;
        let bottom = self.pos.y;

        graphics::Rect::new(left, top, right - left, bottom - top)
    }
}

pub struct Ball {
    pub pos: Point2<f32>,
    pub is_alive: bool,
    pub velocity: Vector2<f32>,
    width: f32,
    height: f32,
}

impl Ball {
    pub fn new(pos: Point2<f32>, width: f32, height: f32) -> Self {
        Ball {
            pos,
            is_alive: true,
            velocity: Vector2 {
                //TODO: maybe export this as variable on creation
                x: -250.0,
                y: -250.0,
            },
            width: width,
            height: height,
        }
    }

    pub fn update(&mut self, seconds: f32, screen_width: f32) {
        let mut next_pos_x = self.pos.x + self.velocity.x * seconds;
        let mut next_pos_y = self.pos.y + self.velocity.y * seconds;

        // Check if ball is near walls of the game - if so bounce
        if next_pos_x < 0.0 {
            self.velocity.x = self.velocity.x * -1.0;
            next_pos_x = self.pos.x + self.velocity.x * seconds;
        }

        if next_pos_y < 0.0 {
            self.velocity.y = self.velocity.y * -1.0;
            next_pos_y = self.pos.y + self.velocity.y * seconds;
        }

        if next_pos_x > screen_width {
            self.velocity.x = self.velocity.x * -1.0;
            next_pos_x = self.pos.x + self.velocity.x * seconds;
        }

        self.pos.x = next_pos_x;
        self.pos.y = next_pos_y;
    }

    pub fn draw(&mut self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        graphics::draw(
            ctx,
            &assets.ball_image,
            graphics::DrawParam::default().dest(self.pos),
        )
    }

    pub fn get_width(&self) -> f32 {
        return self.width;
    }

    pub fn get_height(&self) -> f32 {
        return self.height;
    }
}

pub struct Block {
    pub pos: Point2<f32>,
    pub is_alive: bool,
    life: i32,
    width: f32,
    height: f32,
    is_dmged: bool,
    inital_life: i32,
}

impl Block {
    pub fn new(pos: Point2<f32>, width: f32, height: f32, life: i32) -> Self {
        Block {
            pos,
            life,
            width,
            height,
            is_alive: true,
            is_dmged: false,
            inital_life: life,
        }
    }

    pub fn draw(&mut self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        match self.inital_life {
            1 => graphics::draw(
                ctx,
                &assets.block_image,
                graphics::DrawParam::default().dest(self.pos),
            ),
            2 => match self.life {
                2 => graphics::draw(
                    ctx,
                    &assets.block_orange_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
                1 => graphics::draw(
                    ctx,
                    &assets.block_orange_dmged_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
                _ => graphics::draw(
                    ctx,
                    &assets.block_metal_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
            },
            3 => match self.life {
                3 => graphics::draw(
                    ctx,
                    &assets.block_red_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
                2 => graphics::draw(
                    ctx,
                    &assets.block_red_lowdmg_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
                1 => graphics::draw(
                    ctx,
                    &assets.block_red_dmged_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
                _ => graphics::draw(
                    ctx,
                    &assets.block_metal_image,
                    graphics::DrawParam::default().dest(self.pos),
                ),
            },
            _ => graphics::draw(
                ctx,
                &assets.block_metal_image,
                graphics::DrawParam::default().dest(self.pos),
            ),
        }
    }

    pub fn bounding_rect(&self) -> graphics::Rect {
        let left = self.pos.x;
        let right = self.pos.x + self.width;
        let top = self.pos.y;
        let bottom = self.pos.y + self.height;

        graphics::Rect::new(left, top, right - left, bottom - top)
    }

    pub fn deal_dmg(&mut self, dmg: i32) {
        self.is_dmged = true;
        self.life -= dmg;
        if self.life <= 0 {
            self.is_alive = false;
        }
    }
}
