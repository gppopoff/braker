use ggez::graphics;
use ggez::{Context, GameResult};

pub struct Assets {
    pub ferris_normal_image: graphics::Image,
    pub ball_image: graphics::Image,
    pub block_image: graphics::Image,
    pub block_dmged_image: graphics::Image,
    pub block_metal_image: graphics::Image,
    pub block_orange_image: graphics::Image,
    pub block_orange_dmged_image: graphics::Image,
    pub block_red_image: graphics::Image,
    pub block_red_lowdmg_image: graphics::Image,
    pub block_red_dmged_image: graphics::Image,
}

impl Assets {
    pub fn new(ctx: &mut Context) -> GameResult<Assets> {
        let ferris_normal_image = graphics::Image::new(ctx, "/ferris-normal.png")?;
        let ball_image = graphics::Image::new(ctx, "/ball.png")?;
        let block_image = graphics::Image::new(ctx, "/block.png")?;
        let block_dmged_image = graphics::Image::new(ctx, "/block_dmged.png")?;
        let block_metal_image = graphics::Image::new(ctx, "/block_metal.png")?;
        let block_orange_image = graphics::Image::new(ctx, "/block_orange.png")?;
        let block_orange_dmged_image = graphics::Image::new(ctx, "/block_orange_dmged.png")?;
        let block_red_image = graphics::Image::new(ctx, "/block_red.png")?;
        let block_red_lowdmg_image = graphics::Image::new(ctx, "/block_red_lowdmg.png")?;
        let block_red_dmged_image = graphics::Image::new(ctx, "/block_red_dmged.png")?;

        Ok(Assets {
            ferris_normal_image,
            ball_image,
            block_image,
            block_dmged_image,
            block_metal_image,
            block_orange_image,
            block_orange_dmged_image,
            block_red_image,
            block_red_lowdmg_image,
            block_red_dmged_image,
        })
    }
}
